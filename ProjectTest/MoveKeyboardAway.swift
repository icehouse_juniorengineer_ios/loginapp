//
//  MoveKeyboardAway.swift
//  ProjectTest
//
//  Created by Jeriko on 12/26/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

extension MainPageViewController {
    func moveKeyboardAway(){
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y = 0
        }
    }
}
