//
//  SectionCollapse.swift
//  ProjectTest
//
//  Created by Jeriko on 1/5/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation

extension TableViewDelegate {
    
    func sectionRemove(indexPath: IndexPath, tableView: UITableView){
        var newIndexpath = indexPath
        
        tableView.beginUpdates()
        
//        newIndexpath.row = newIndexpath.row + 1
//        tableView.deleteRows(at: [newIndexpath], with: UITableViewRowAnimation.bottom)
//        
//        TableSectionStruct.data[indexPath.section].remove(at: indexPath.row)
//        TableSectionStruct.openedCell.remove(at: newIndexpath.row) // remove child row
//        
//        TableSectionStruct.data[indexPath.section].remove(at: indexPath.row)
//        TableSectionStruct.openedCell.remove(at: newIndexpath.row) // remove child row
//        
//        TableSectionStruct.data[indexPath.section].remove(at: indexPath.row)
//        TableSectionStruct.openedCell.remove(at: newIndexpath.row) // remove child row
//        
//        newIndexpath.row = newIndexpath.row + 1
//        tableView.deleteRows(at: [newIndexpath], with: UITableViewRowAnimation.bottom) // this ****** has different update timing of index when inside begin/end update
//        
//        newIndexpath.row = newIndexpath.row + 1
//        tableView.deleteRows(at: [newIndexpath], with: UITableViewRowAnimation.bottom)
        
        for i in (newIndexpath.row..<(newIndexpath.row + 3)).reversed() {
            tableView.deleteRows(at: [newIndexpath], with: UITableViewRowAnimation.bottom)
            TableSectionStruct.data[indexPath.section].remove(at: i)
            TableSectionStruct.openedCell.remove(at: i)
        }
        
        
        tableView.endUpdates()
    }
}
