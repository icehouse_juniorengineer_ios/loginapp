//
//  TableStruct.swift
//  ProjectTest
//
//  Created by Jeriko on 1/5/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation
import UIKit

struct TableSectionStruct {
    //    static var data = [[String]]()
    static var data = [
        ["dummy string"],
        ["dummy string"],
        ["dummy string"]
    ]
    
    static var data02 = [
        ["dummy string"],
        ["dummy string"],
        ["dummy string"]
    ]
    static var SectionTitle = ["Members","Home","Phone Number"]
    static var openedCell = [Int]()
    static var dummyDataCell = ["1","2"]
    static var contactStatus = Bool()
    static var HeaderType = Int()
    static var currentArrow = [UIImage]()
    static var isSectionOpened = [Bool](repeating: true, count: 3)
    static var currentSection = Int()
    static var mainTableView = UITableView()
    static var touchLocation = CGPoint()
}
