//
//  TabBar.swift
//  ProjectTest
//
//  Created by Jeriko on 12/26/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit
import MapKit

class TabBar: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup before the view appears.
        
//        self.tabBar.alpha = 1.0
//        self.tabBar.isTranslucent = true
        
        self.tabBar.tintColor = UIColor.white
        self.tabBar.barTintColor = UIColor.darkGray
        
        
        // Create Tab one
        let tabOne = TableSectionViewController()
        let tabOneBarItem = UITabBarItem(title: "Data", image: UIImage(named: "circle_black.png"), selectedImage: UIImage(named: "circle_black.png"))
        tabOne.tabBarItem = tabOneBarItem
        
        
        
        // Create Tab two
//        let tabTwo = classes.MapViewClass
        let tabTwo = MapView()
        let tabTwoBarItem2 = UITabBarItem(title: "Position", image: UIImage(named: "circle_black.png"), selectedImage: UIImage(named: "circle_black.png"))
        tabTwo.tabBarItem = tabTwoBarItem2
        
        // Create Tab three
        let tabThree = TimerDate()
        let tabThreeBarItem = UITabBarItem(title: "Test", image: UIImage(named: "circle_black.png"), selectedImage: UIImage(named: "circle_black.png"))
        tabThree.tabBarItem = tabThreeBarItem
        
        self.viewControllers = [tabOne, tabTwo, tabThree]
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
