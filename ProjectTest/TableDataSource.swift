//
//  TableDataSource.swift
//  ProjectTest
//
//  Created by Jeriko on 12/22/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class TableViewDataSource: NSObject, UITableViewDataSource {
    
    
    
    /////////////////////////////////
    //sections
    /////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableSectionStruct.SectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TableSectionStruct.SectionTitle[section]
    }
    
    
    //////////////////////////////////
    //rows
    //////////////////////////////////
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableSectionStruct.data[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //////////////////////////////////
        //populate child rows
        //////////////////////////////////
        if indexPath.section == 0 && TableSectionStruct.openedCell[indexPath.row] == 2{
            let cellChild : ChildCellTwo = tableView.dequeueReusableCell(withIdentifier: "childcelltwo", for: indexPath) as! ChildCellTwo
            let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
            let tempString = PublicStruct.NewEmailStaticData[indexPath.row-(indexForChild)]
            cellChild.childCelltTwo.text = ViewControllerTable.getName(filteredName: tempString)
            return cellChild
            
        } else if indexPath.section == 0 && TableSectionStruct.openedCell[indexPath.row] == 3{
            let cellChild : ChildCell = tableView.dequeueReusableCell(withIdentifier: "child", for: indexPath) as! ChildCell
            let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
            cellChild.labelChild.text = PublicStruct.NewDate[indexPath.row-(indexForChild)]
            return cellChild
            
        } else if indexPath.section == 0 && TableSectionStruct.openedCell[indexPath.row] == 4{
            let cellChild : EmailCell = tableView.dequeueReusableCell(withIdentifier: "emailcell", for: indexPath) as! EmailCell
            let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
            cellChild.textLabel?.text = ViewControllerMainPage.EmailData[indexPath.row-(indexForChild)]
            return cellChild
            
            //////////////////////////////////
            //populatte child rows end
            //////////////////////////////////
            
        } else {
            
            let cell : TableSectionViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableSectionViewCell
            
            if indexPath.section == 0 {
                let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
                let tempString = PublicStruct.NewEmailStaticData[indexPath.row-(indexForChild)]
                cell.memberLabel.text = "Member \(PublicStruct.NewMemberData[indexPath.row-(indexForChild)]) : \(ViewControllerTable.getName(filteredName: tempString))"
                cell.arrowImage.image = TableSectionStruct.currentArrow[indexPath.row-(indexForChild)]
            } else {
                cell.memberLabel.text = TableSectionStruct.data[indexPath.section][indexPath.row]
                cell.arrowImage.image = nil
            }
            
            switch indexPath.section{
            case 0:
                cell.backgroundColor = UIColor(red: 234/255, green: 250/255, blue: 255/255, alpha: 1)
            case 1:
                cell.backgroundColor = UIColor(red: 201/255, green: 252/255, blue: 255/255, alpha: 1)
            case 2:
                cell.backgroundColor = UIColor(red: 116/255, green: 226/255, blue: 232/255, alpha: 1)
            default:
                cell.backgroundColor = UIColor.clear
            }
            return cell
        }
    }
}
