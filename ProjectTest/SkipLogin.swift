//
//  SkipLogin.swift
//  ProjectTest
//
//  Created by Jeriko on 12/26/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

extension MainPageViewController{
    func skipLogin (callerView: UIViewController){
        
        /////////////////////////////////
        //      set data for members
        /////////////////////////////////
        if PublicStruct.NewMemberData.count != 0 {
            setDataMembers(caller: "skipLogin")
            
            print(PublicStruct.NewMemberData)
        } else {
            print("population empty")
        }
        
        /////////////////////////////////
        //      set data for email sections
        /////////////////////////////////
        ViewControllerMainPage.setDataEmail(appending: "nil")
        
        /////////////////////////////////
        //      set data for phone sections
        /////////////////////////////////
        ViewControllerMainPage.setDataPhone(appending: "nil")
        
        /////////////////////////////////
        //      set data for registration date
        /////////////////////////////////
        ViewControllerMainPage.arraySetupForSetDate(currentDate: "nil")
        
        
        /////////////////////////////////
        //      set data for random coordinate
        /////////////////////////////////
        setRandomCoordinate(appending: "nil")
    }
    
    
    
}
