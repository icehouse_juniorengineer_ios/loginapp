//
//  UINavConViewController.swift
//  ProjectTest
//
//  Created by Jeriko on 12/6/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class UINavConViewController: UINavigationController {
    
    let navDelegate = NCDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = navDelegate
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
