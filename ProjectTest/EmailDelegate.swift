//
//  TextFieldDelegate.swift
//  ProjectTest
//
//  Created by Jeriko on 12/27/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation
import UIKit

class EmailFieldDelegate: NSObject, UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if PublicStruct.emailStrCount < PublicStruct.emailStrMaxCount{
            ViewControllerMainPage.nameFieldCheck(fieldName: textField.text!, input: string)
            buttonEnabler()
            return true
        }
        
        if PublicStruct.emailStrCount == PublicStruct.emailStrMaxCount && string == "" {
            ViewControllerMainPage.nameFieldCheck(fieldName: textField.text!, input: string)
            buttonEnabler()
            return true
        }
        return false
    }
}
