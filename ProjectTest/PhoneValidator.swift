//
//  PhoneValidator.swift
//  ProjectTest
//
//  Created by Jeriko on 12/6/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation

class EmailValidator {
    static func isValid (str:String) -> Bool {
        let emailregex = "[A-Za-z0-9._]{3,30}+@[A-Za-z]{3,15}+\\.[A-Za-z0-9]{2,6}"
        let emailvalidation = NSPredicate(format:"SELF MATCHES %@", emailregex)
        return emailvalidation.evaluate(with: str)
    }
}
