//
//  Extension.swift
//  ProjectTest
//
//  Created by Jeriko on 12/21/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

extension UIViewController {
    // Remove the given characters in the range
    public func backgroundGradient()
    {
        ForSubLayers.sublayer = CAGradientLayer()
        ForSubLayers.sublayer.frame = self.view.bounds
        ForSubLayers.sublayer.colors = [
            Helper.gradientTopColor(),
            Helper.gradientBottomColor()]
        
        self.view.layer.insertSublayer(ForSubLayers.sublayer, at: 0)
    }
    
    public func updateBackgroundGradientSize(viewHeight: CGFloat, viewWidth: CGFloat){
        let newSize = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        ForSubLayers.sublayer.frame = newSize
        ForSubLayers.sublayer.layoutSublayers()
    }
}

extension UIView {
    // Remove the given characters in the range
    public func backgroundGradient()
    {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [
            Helper.gradientTopColor(),
            Helper.gradientBottomColor()]
        self.layer.insertSublayer(gradient, at: 0)
    }
}
