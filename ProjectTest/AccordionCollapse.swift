//
//  AccordionAdd.swift
//  ProjectTest
//
//  Created by Jeriko on 1/5/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation

extension TableViewDelegate {
    
    func accordionRemove(indexPath: IndexPath, tableView: UITableView){
        var newIndexpath = indexPath
        
        // tableView.beginUpdates()
        tableView.beginUpdates()
        
        newIndexpath.row = newIndexpath.row + 1
        tableView.deleteRows(at: [newIndexpath], with: UITableViewRowAnimation.bottom)
        
        TableSectionStruct.data[indexPath.section].remove(at: indexPath.row)
        TableSectionStruct.openedCell.remove(at: newIndexpath.row) // remove child row
        
        TableSectionStruct.data[indexPath.section].remove(at: indexPath.row)
        TableSectionStruct.openedCell.remove(at: newIndexpath.row) // remove child row
        
        TableSectionStruct.data[indexPath.section].remove(at: indexPath.row)
        TableSectionStruct.openedCell.remove(at: newIndexpath.row) // remove child row
        
        newIndexpath.row = newIndexpath.row + 1
        tableView.deleteRows(at: [newIndexpath], with: UITableViewRowAnimation.bottom) // this ****** has different update timing of index when inside begin/end update
        
        newIndexpath.row = newIndexpath.row + 1
        tableView.deleteRows(at: [newIndexpath], with: UITableViewRowAnimation.bottom)
        
        
        tableView.endUpdates()
    }
}
        
