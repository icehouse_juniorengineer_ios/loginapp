//
//  MainPageViewController.swift
//  ProjectTest
//
//  Created by Jeriko on 11/30/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController {
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        PublicStruct.viewLifeCycle.append("viewWillDisappear")
        PublicStruct.viewLifeCycleTime.append(NSDate.timeIntervalSinceReferenceDate)
        Debugger.timeStamp(logmessage: "viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
        PublicStruct.viewLifeCycle.append("viewDidDisappear")
        PublicStruct.viewLifeCycleTime.append(NSDate.timeIntervalSinceReferenceDate)
        Debugger.timeStamp(logmessage: "viewDidDisappear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        PublicStruct.viewLifeCycle.append("viewDidAppear")
        PublicStruct.viewLifeCycleTime.append(NSDate.timeIntervalSinceReferenceDate)
        Debugger.timeStamp(logmessage: "viewDidAppear")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        PublicStruct.viewLifeCycle.append("viewWillLayoutSubviews")
        PublicStruct.viewLifeCycleTime.append(NSDate.timeIntervalSinceReferenceDate)
        Debugger.timeStamp(logmessage: "viewWillLayoutSubviews")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        PublicStruct.viewLifeCycle.append("viewDidLayoutSubviews")
        PublicStruct.viewLifeCycleTime.append(NSDate.timeIntervalSinceReferenceDate)
        Debugger.timeStamp(logmessage: "viewDidLayoutSubviews")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        PublicStruct.viewLifeCycle.append("viewWillAppear")
        PublicStruct.viewLifeCycleTime.append(NSDate.timeIntervalSinceReferenceDate)
        Debugger.timeStamp(logmessage: "viewWillAppear")
    }
    
    /////////////////////////////////
    //      View code starts here
    /////////////////////////////////
    
    let EmailTextDelegate = EmailFieldDelegate()
    let PhoneTextDelegate = PhoneFieldDelegate()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PublicStruct.viewLifeCycle.append("viewDidLoad")
        PublicStruct.viewLifeCycleTime.append(NSDate.timeIntervalSinceReferenceDate)
        Debugger.timeStamp(logmessage: "viewDidLoad")
        
        // Do any additional setup after loading the view.
        
        
        /////////////////////////////////
        //      textField
        /////////////////////////////////
        
        field1.delegate = EmailTextDelegate
        field2.delegate = PhoneTextDelegate
        
        TextFieldStruct.MainPage = self
        
        TextFieldStruct.buttonAlpha = loginButtonOutlet.alpha
        TextFieldStruct.buttonState = loginButtonOutlet.isEnabled
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateLoginButtonState),
            name: NSNotification.Name(rawValue: "stateupdated"),
            object: nil)
        
        field1.addTarget(
            self,
            action: #selector(UITextFieldDelegate.textField(_:shouldChangeCharactersIn:replacementString:)),
            for: UIControlEvents.valueChanged)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasShown),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil)
        
        leftViewPadding(textField: field2)
        
        /////////////////////////////////
        //      other
        /////////////////////////////////
        
        loginButtonOutlet.isEnabled = false
        loginButtonOutlet.alpha = 0.1
        
        PublicStruct.field1Yaxis = field1.frame.origin.y
        PublicStruct.field2Yaxis = field2.frame.origin.y
        
        self.backgroundGradient()
        
        self.title = "Login Page"
        
        errorLabel.text = " "
        
        setDataMembers(caller: "skipLogin")
        
        /////////////////////////////////
        //      button border
        /////////////////////////////////
        clearButtonOutlet.layer.cornerRadius = 0.5 * clearButtonOutlet.bounds.size.width
        clearButtonOutlet.layer.borderColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:1).cgColor as CGColor
        clearButtonOutlet.layer.borderWidth = 2.0
        clearButtonOutlet.clipsToBounds = true
        
        
        /////////////////////////////////
        //      constraints setup
        /////////////////////////////////
        Constraints.field2X = field2X.constant
        Constraints.field2Y = field2Y.constant
        
        Constraints.phoneX = phoneNumberX.constant
        Constraints.phoneY = phoneNumberY.constant
        
        /////////////////////////////////
        //      tests goes here
        /////////////////////////////////
        
        
    }
    
    /////////////////////////////////
    //      check device orientation
    /////////////////////////////////
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let viewHeight = self.view.frame.size.width
        let viewWidth = self.view.frame.size.height
        
        if UIDevice.current.orientation.isPortrait{
            field2X.constant = Constraints.field2X
            field2Y.constant = Constraints.field2Y
            
            phoneNumberX.constant = Constraints.phoneX
            phoneNumberY.constant = Constraints.phoneY
            
            updateBackgroundGradientSize(viewHeight: viewHeight, viewWidth: viewWidth)
            
        } else if UIDevice.current.orientation.isLandscape{
            
            field2X.constant = Constraints.field2X - ((viewWidth/2)*0.55)
            field2Y.constant = Constraints.field2Y
            
            phoneNumberX.constant = Constraints.phoneX - ((viewWidth/2)*0.55)
            phoneNumberY.constant = Constraints.phoneY
            
            updateBackgroundGradientSize(viewHeight: viewHeight, viewWidth: viewWidth)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var field1: UITextField!
    @IBOutlet weak var field2: UITextField!
    
    @IBOutlet weak var field2Y: NSLayoutConstraint!
    @IBOutlet weak var field2X: NSLayoutConstraint!
    
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var phoneNumberX: NSLayoutConstraint!
    @IBOutlet weak var phoneNumberY: NSLayoutConstraint!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var loginButtonOutlet: UIButton!
    
    @IBOutlet weak var clearButtonOutlet: UIButton!
    
    
    
    @IBAction func skipLogin(_ sender: UIButton) {
        skipLogin(callerView: self)
        //            ViewControllerMainPage.presentTableView(callerView: self)
        classes.tabBarController = TabBar()
        presentController(callerView: self, toBeCalled: classes.tabBarController)
        TableSectionStruct.HeaderType = 0 //untuk gonta ganti header
        TableSectionStruct.data02 = TableSectionStruct.data
    }
    
    @IBAction func loginButton2(_ sender: Any) {
        field1.resignFirstResponder()
        field2.resignFirstResponder()
        
        TableSectionStruct.HeaderType = 0
        
        if EmailValidator.isValid(str: field1.text!) &&
            PhoneValidator.isValid(str: field2.text!) &&
            ViewControllerMainPage.emailNotExistCheck(string: field1.text!){
            
            setDataMembers(caller: "any")
            ViewControllerMainPage.setDataEmail(appending: field1.text!)
            
            let phoneNumber = "0" + field2.text!
            ViewControllerMainPage.setDataPhone(appending: phoneNumber)
            
            ViewControllerMainPage.setDate()
            setRandomCoordinate(appending: "not nil")
            
            /////////////////////////////////
            //      set data end
            /////////////////////////////////
            
            errorLabel.text = " "
            
            //            ViewControllerMainPage.presentTableView(callerView: self)
            
            classes.tabBarController = TabBar()
            TableSectionStruct.data02 = TableSectionStruct.data
            presentController(callerView: self, toBeCalled: classes.tabBarController)
        } else {
            if ViewControllerMainPage.emailNotExistCheck(string: field1.text!){
                errorLabel.text = "Invalid email address or phone number"
            } else {
                errorLabel.text = "Email already Exist"
            }
        }
    }
    
    @IBAction func clearUserData(_ sender: UIButton) {
        ViewControllerMainPage.clearData()
    }
    
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        field1.resignFirstResponder()
        field2.resignFirstResponder()
    }
    
    
    /////////////////////////////////
    //      update from UITextFieldDelegate
    /////////////////////////////////
    
    func updateLoginButtonState (){
        loginButtonOutlet.isEnabled = TextFieldStruct.buttonState
        loginButtonOutlet.alpha = TextFieldStruct.buttonAlpha
    }
    
    /////////////////////////////////
    //      NSNotificationCenter.adObservers() - keyboard Functions
    /////////////////////////////////
    
    func keyboardWasShown(notification: NSNotification)
    {
        
        var frameYaxis = CGFloat()
        var textFieldHeight = CGFloat()
        var responder = Bool()
        
        if field1.isFirstResponder{
            frameYaxis = field1.frame.origin.y
            textFieldHeight = field1.frame.height
            responder = true
        }
        
        if field2.isFirstResponder{
            frameYaxis = field2.frame.origin.y
            textFieldHeight = field1.frame.height
            responder = true
        }
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        moveKeyboardUp(keyboardSize: keyboardSize!, frameYaxis: frameYaxis, textFieldHeight: textFieldHeight, responder: responder)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        moveKeyboardAway()
    }
    
}
