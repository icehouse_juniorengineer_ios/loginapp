//
//  SetRandomCoordinate.swift
//  ProjectTest
//
//  Created by Jeriko on 1/3/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import CoreLocation

extension MainPageViewController{
    
    func setRandomCoordinate(appending: String){
        
        var NSUserDataLatitude = [CGFloat]()
        var NSUserDataLongitude = [CGFloat]()
        var location = CLLocation()
        let mapStruct = MapStruct()
        
        if let temp01 = PublicStruct.PersistentData.array(forKey: "randomLatitude") as? [CGFloat] {
            NSUserDataLatitude = temp01
        }
        
        if let temp02 = PublicStruct.PersistentData.array(forKey: "randomLongitude") as? [CGFloat] {
            NSUserDataLongitude = temp02
        }
        
        let randomLatitude = mapStruct.randomCGFloat(min: mapStruct.minLat, max: mapStruct.maxLat)
        let randomLongitude = mapStruct.randomCGFloat(min: mapStruct.minLon, max: mapStruct.maxLon)
        
        mapStruct.homesPopulator(latitude: NSUserDataLatitude, longitude: NSUserDataLongitude, count: NSUserDataLatitude.count-1)
        
        if appending != "nil" {
            NSUserDataLatitude.append(randomLatitude)
            NSUserDataLongitude.append(randomLongitude)
            mapStruct.homesAppender(latitude: randomLatitude, longitude: randomLongitude)
        }
        
        PublicStruct.PersistentData.set(NSUserDataLatitude, forKey: "randomLatitude")
        PublicStruct.PersistentData.set(NSUserDataLongitude, forKey: "randomLongitude")
        
        if NSUserDataLatitude.count >= 0{
            for i in 0...NSUserDataLatitude.count - 1 {
                
                if MapStruct.homesName.count != NSUserDataLatitude.count {
                    MapStruct.homesName.append(" ")
                }
                
                location = CLLocation(latitude: CLLocationDegrees(NSUserDataLatitude[i]), longitude: CLLocationDegrees(NSUserDataLongitude[i]))
                
                let geofront = CLGeocoder()
                
                geofront.reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
                    var placemark:CLPlacemark!
                    
                    if error == nil && (placemarks?.count)! > 0 {
                        placemark = (placemarks?[0])! as CLPlacemark
                        
                        var addressString : String = ""
                        if placemark.isoCountryCode == "TW" /*Address Format in Chinese*/ {
                            if placemark.country != nil {
                                addressString = placemark.country!
                            } else {
                                print("country doesn't exist")
                            }
                            if placemark.subAdministrativeArea != nil {
                                addressString = addressString + placemark.subAdministrativeArea! + ", "
                            }
                            if placemark.postalCode != nil {
                                addressString = addressString + placemark.postalCode! + " "
                            }
                            if placemark.locality != nil {
                                addressString = addressString + placemark.locality!
                            }
                            if placemark.thoroughfare != nil {
                                addressString = addressString + placemark.thoroughfare!
                            }
                            if placemark.subThoroughfare != nil {
                                addressString = addressString + placemark.subThoroughfare!
                            }
                        } else {
                            if placemark.subThoroughfare != nil {
                                addressString = placemark.subThoroughfare! + " "
                            }
                            if placemark.thoroughfare != nil {
                                addressString = addressString + placemark.thoroughfare! + ", "
                            }
                            if placemark.postalCode != nil {
                                addressString = addressString + placemark.postalCode! + " "
                            }
                            if placemark.locality != nil {
                                addressString = addressString + placemark.locality! + ", "
                            }
                            if placemark.administrativeArea != nil {
                                addressString = addressString + placemark.administrativeArea! + " "
                            }
                            if placemark.country != nil {
                                addressString = addressString + placemark.country!
                            } else {
                                print("country doesn't exist")
                            }
                        }
                        MapStruct.homesName[i] = addressString
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "maplocationupdated"), object: self)
                    }
                }
                )
            }
        }
        TableSectionStruct.data[1] = MapStruct.homesName
    }
}
