//
//  TableViewSection.swift
//  ProjectTest
//
//  Created by Jeriko on 12/8/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class TableViewSection: UITableViewCell {
    
    var currentSection = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.isUserInteractionEnabled = true
        
//        let headerTapGesture = UITapGestureRecognizer()
//        headerTapGesture.addTarget(self,action: #selector(tapHeader))
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                         action: #selector(tapHeader)))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBOutlet weak var sectionTitle: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? TableViewSection else {
            print("tap failed")
            return
        }
        print(cell)
    }
}
