//
//  GeoCoder.swift
//  ProjectTest
//
//  Created by Jeriko on 12/29/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit
import CoreLocation

class GeoCoder: CLGeocoder {

    override func reverseGeocodeLocation(_ location: CLLocation, completionHandler: @escaping CLGeocodeCompletionHandler) {
        super.reverseGeocodeLocation(location, completionHandler: completionHandler)
    }
    
}
