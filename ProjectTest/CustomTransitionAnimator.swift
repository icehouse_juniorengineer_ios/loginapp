//
//  CustomTransitionAnimator.swift
//  ProjectTest
//
//  Created by Jeriko on 12/16/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class CustomTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning, CAAnimationDelegate {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    weak var transitionContext: UIViewControllerContextTransitioning?
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        //1
        self.transitionContext = transitionContext
        
        //2
        let containerView = transitionContext.containerView
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
//            as! MainPageViewController
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
//            as! TableSectionViewController
//        let button = fromViewController.loginButtonOutlet!
        
        //3
        containerView.addSubview(toViewController!.view)
        
        //4
        let midY = fromViewController!.view.frame.height / 2
        let midX = fromViewController!.view.frame.width / 2
        let middle = CGRect(x: midX, y: midY, width: 1.0, height: 1.0)
        
        let circleMaskPathInitial = UIBezierPath(ovalIn: middle)
        let extremePoint = CGPoint(x: fromViewController!.view.center.x - 0, y: fromViewController!.view.center.y - 0)
        let radius = sqrt((extremePoint.x*extremePoint.x) + (extremePoint.y*extremePoint.y))
        let circleMaskPathFinal = UIBezierPath(ovalIn: fromViewController!.view.frame.insetBy(dx: -radius, dy: -radius))
        
        //5
        let maskLayer = CAShapeLayer()
        maskLayer.path = circleMaskPathFinal.cgPath
        toViewController!.view.layer.mask = maskLayer
        
        //6
        let maskLayerAnimation = CABasicAnimation(keyPath: "path")
        maskLayerAnimation.fromValue = circleMaskPathInitial.cgPath
        maskLayerAnimation.toValue = circleMaskPathFinal.cgPath
        maskLayerAnimation.duration = self.transitionDuration(using: transitionContext)
        maskLayerAnimation.delegate = self
        maskLayer.add(maskLayerAnimation, forKey: "path")
    }
    
   func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.transitionContext?.completeTransition(!self.transitionContext!.transitionWasCancelled)
        self.transitionContext?.viewController(forKey: UITransitionContextViewControllerKey.from)?.view.layer.mask = nil
    }
    
    
}
