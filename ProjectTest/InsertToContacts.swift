//
//  InsertToContacts.swift
//  ProjectTest
//
//  Created by Jeriko on 1/3/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import MapKit
import Contacts
import ContactsUI

extension TableViewDelegate{
    func insertToContacts(index: Int){
        //            UIApplication.shared.open(URL(string: "tel://\(TableSectionStruct.data[2][indexPath.row])")!, options: [:], completionHandler: nil)
        let addNewContact = CNContactStore()
        let saveContact = CNSaveRequest()
        let newContact = CNMutableContact()
        let authStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        /////////////////////////////////////////////////////
        //      Prepares email
        /////////////////////////////////////////////////////
        let email = CNLabeledValue(label: CNLabelWork, value: PublicStruct.NewEmailStaticData[index] as NSString)
        newContact.emailAddresses = [email]
        
        /////////////////////////////////////////////////////
        //      Prepares phone
        /////////////////////////////////////////////////////
        let phone = CNLabeledValue(label: CNLabelPhoneNumberMain, value: CNPhoneNumber(stringValue: PublicStruct.NewPhoneStaticData[index]))
        newContact.phoneNumbers = [phone]
        
        /////////////////////////////////////////////////////
        //      Prepares postalAddress
        /////////////////////////////////////////////////////
        let mutablePostal = CNMutablePostalAddress()
        mutablePostal.street = MapStruct.homesName[index]
        let postalAddress = CNLabeledValue(label: CNLabelHome, value: mutablePostal)
        newContact.postalAddresses = [postalAddress as! CNLabeledValue<CNPostalAddress>]
        
        
        newContact.givenName = ViewControllerTable.getName(filteredName: PublicStruct.NewEmailStaticData[index])
        let predicate = CNContact.predicateForContacts(matchingName: ViewControllerTable.getName(filteredName: PublicStruct.NewEmailStaticData[index]))
        let tofetch = [CNContactGivenNameKey]
        
        do {
            let arrayContact = try addNewContact.unifiedContacts(matching: predicate, keysToFetch: tofetch as [CNKeyDescriptor])
            if arrayContact.count > 0 {
                TableSectionStruct.contactStatus = true
                //                    let existingContact = arrayContact.first
                //                    let mutableContact = existingContact?.mutableCopy() as! CNMutableContact
                //                    saveContact.delete(mutableContact)
                //                    do {
                //                        try addNewContact.execute(saveContact)
                //                    } catch {
                //                        print(error)
                //                    }
            } else {
                TableSectionStruct.contactStatus = false
                print(arrayContact.count)
                saveContact.add(newContact, toContainerWithIdentifier: nil)
                do {
                    try addNewContact.execute(saveContact)
                } catch {
                    print(error)
                }
            }
        } catch {
            print(error)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "contactupdated"), object: self)
    }
    
}
