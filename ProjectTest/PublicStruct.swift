//
//  PublicStruct.swift
//  ProjectTest
//
//  Created by Jeriko on 11/30/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation
import UIKit

struct PublicStruct{
    static var field1Yaxis = CGFloat()
    static var field2Yaxis = CGFloat()
    
    static var emailStrCount = 0
    static var emailStr = String()
    static let emailStrMaxCount = 64
    static var emailFieldFilled = false
    
    static var phoneStrCount = 0
    static var phoneStr = String()
    static let phoneStrMaxCount = 12
    static var phoneFieldFilled = false
    
    static let EmailStaticDataKey = "emailstaticdatakey"
    static var OldEmailStaticData = [String]()
    static var NewEmailStaticData = [String]()
    
    static let PhoneStaticDataKey = "phonestaticdatakey"
    static var OldPhoneStaticData = [String]()
    static var NewPhoneStaticData = [String]()
    
    static let MemberStaticDataKey = "memberstaticdatakey"
    static var OldMemberStaticData = [Int]()
    static var NewMemberStaticData = [Int]()
    static var NewMemberData = [String]()
    static let MemberDataIndexKey = "memberstaticdataindexkey"
    static var OldMemberDataIndex = 0
    static var NewMemberDataIndex = 0
    
    static var viewLifeCycle = [String]()
    static var viewLifeCycleTime = [Double]()
    
    static var PersistentData = UserDefaults.standard
    
    static let DateKey = "datekey"
    static var OldDate = [String]()
    static var NewDate = [String]()
}

struct TempData {
    static var tempArray = [String]()
}

struct Constraints {
    static var phoneX = CGFloat()
    static var phoneY = CGFloat()
    static var field2Y = CGFloat()
    static var field2X = CGFloat()
}

struct classes {
    static var tabBarController = UITabBarController()
    static var mainNavigationController = UINavigationController()
//    static var MapViewClass = MapView()
}

