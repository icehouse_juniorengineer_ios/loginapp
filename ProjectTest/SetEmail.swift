//
//  SetEmail.swift
//  ProjectTest
//
//  Created by Jeriko on 1/3/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation
extension ViewControllerMainPage{
    
    static func setDataEmail (appending: String) {
        
        /////////////////////////////////
        //      set data for email sections
        /////////////////////////////////
        
        PublicStruct.OldEmailStaticData = PublicStruct.PersistentData.stringArray(forKey: PublicStruct.EmailStaticDataKey) ?? [String]() //get nsuser data to array
        PublicStruct.NewEmailStaticData = PublicStruct.OldEmailStaticData //get previous nsuserdefault data
        if appending != "nil" {
            PublicStruct.NewEmailStaticData.append(appending) //append the new data to the old array
        }
        PublicStruct.PersistentData.set(PublicStruct.NewEmailStaticData, forKey: PublicStruct.EmailStaticDataKey)
        
        EmailData = PublicStruct.NewEmailStaticData
    }
}
