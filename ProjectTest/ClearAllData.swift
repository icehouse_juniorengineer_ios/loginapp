//
//  ClearAllData.swift
//  ProjectTest
//
//  Created by Jeriko on 1/3/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation

extension ViewControllerMainPage {
    static func clearData (){
        
        MapStruct.userHomes.removeAll()
        PublicStruct.PersistentData.removeObject(forKey: "randomLatitude")
        PublicStruct.PersistentData.removeObject(forKey: "randomLongitude")
        
        PublicStruct.PersistentData.removeObject(forKey: PublicStruct.DateKey)
        
        PublicStruct.PersistentData.removeObject(forKey: PublicStruct.MemberStaticDataKey)
        PublicStruct.PersistentData.removeObject(forKey: PublicStruct.MemberDataIndexKey)
        PublicStruct.NewMemberDataIndex = 0
        PublicStruct.OldMemberDataIndex = 0
        PublicStruct.OldMemberStaticData.removeAll()
        PublicStruct.NewMemberStaticData.removeAll()
        PublicStruct.NewMemberData.removeAll()
        TableSectionStruct.data[0].removeAll()
        
        PublicStruct.PersistentData.removeObject(forKey: PublicStruct.PhoneStaticDataKey)
        
        PublicStruct.PersistentData.removeObject(forKey: PublicStruct.EmailStaticDataKey)
        
        PublicStruct.PersistentData.synchronize()
    }
}
