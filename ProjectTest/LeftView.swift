//
//  LeftView.swift
//  ProjectTest
//
//  Created by Jeriko on 12/27/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

extension UIViewController {
    func leftViewPadding (textField: UITextField){
        let x = textField.frame.origin.x
        let y = textField.frame.origin.y
        let width = textField.frame.width
        let height = textField.frame.height
        
        let text = UILabel()
        let textRect = CGRect(x: x, y: y, width: width * 0.22, height: height * 1)
        
        text.text = "+62"
        
        text.bounds = textRect
        
        text.textAlignment = .center
        
        text.backgroundColor = UIColor.cyan
        text.layer.cornerRadius = 5
        text.layer.masksToBounds = true
        
        textField.leftView = text
        
        textField.leftViewMode = UITextFieldViewMode.always
    }
}
