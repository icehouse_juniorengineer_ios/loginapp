//
//  EmailValidator.swift
//  ProjectTest
//
//  Created by Jeriko on 12/6/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation

class PhoneValidator {
    static func isValid (str:String) -> Bool {
        let phoneregex = "[0-9]{10,12}"
        let phonevalidation = NSPredicate(format:"SELF MATCHES %@", phoneregex)
        return phonevalidation.evaluate(with: str)
    }
}
