//
//  TextFieldStruct.swift
//  ProjectTest
//
//  Created by Jeriko on 12/27/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation
import UIKit

struct TextFieldStruct {
    static var MainPage = MainPageViewController()
    static var buttonAlpha = CGFloat()
    static var buttonState = Bool()
}
