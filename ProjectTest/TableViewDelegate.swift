//
//  TableViewDelegate.swift
//  ProjectTest
//
//  Created by Jeriko on 12/23/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit
import MapKit
import Contacts
import ContactsUI

class TableViewDelegate: NSObject, UITableViewDelegate {
    
    /////////////////////////////////
    //headers
    /////////////////////////////////
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "sectionCell")
        let sectionHeader = header as! TableViewSection
        
        let headerTwo = tableView.dequeueReusableCell(withIdentifier: "sectioncell")
        let sectionHeaderTwo = headerTwo as! SecondSectionView
        
        switch section{
        case 0:
            sectionHeader.contentView.backgroundColor = UIColor(red: 122/255, green: 165/255, blue: 211/255, alpha: 1)
            sectionHeader.headerImage.image = #imageLiteral(resourceName: "members.png")
        case 1:
            sectionHeader.contentView.backgroundColor = UIColor(red: 110/255, green: 187/255, blue: 191/255, alpha: 1)
            sectionHeader.headerImage.image = #imageLiteral(resourceName: "email.png")
        case 2:
            sectionHeader.contentView.backgroundColor = UIColor(red: 122/255, green: 110/255, blue: 211/255, alpha: 1)
            sectionHeader.headerImage.image = #imageLiteral(resourceName: "phone.png")
            
        default:
            sectionHeader.backgroundColor = UIColor.clear
        }
        
        sectionHeader.sectionTitle.text = TableSectionStruct.SectionTitle[section]
        
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self,action: #selector(tapHeader))
        sectionHeader.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(tapHeader)))
        
        
        TableSectionStruct.currentSection = section
        sectionHeader.currentSection = section
        
        sectionHeader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let containerView = UIView(frame: sectionHeader.frame)
        containerView.addSubview(sectionHeader)
        
        if TableSectionStruct.HeaderType == 0 {
            return containerView
        } else {
            return sectionHeaderTwo.contentView
        }
        
    }
    
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? TableViewSection else {
            print("tap failed")
            return
        }
        
        if TableSectionStruct.isSectionOpened[cell.currentSection] == true {
            
            //clear child cells
//            if cell.currentSection == 0 {
//                
//                for i in (0..<TableSectionStruct.openedCell.count).reversed() {
//                    switch TableSectionStruct.openedCell[i]{
//                    case 1:
//                        TableSectionStruct.openedCell[i] = 0 // set opened state to closed state
//                    case 2:
//                        TableSectionStruct.openedCell.remove(at: i)
//                        TableSectionStruct.data[cell.currentSection].remove(at: i)
//                    case 3:
//                        TableSectionStruct.openedCell.remove(at: i)
//                        TableSectionStruct.data[cell.currentSection].remove(at: i)
//                    case 4:
//                        TableSectionStruct.openedCell.remove(at: i)
//                        TableSectionStruct.data[cell.currentSection].remove(at: i)
//                    default:
//                        break
//                    }
//                }
//            }
            //clear child cells end
            
            TableSectionStruct.data02[cell.currentSection] = TableSectionStruct.data[cell.currentSection]
            
            TableSectionStruct.mainTableView.beginUpdates()
            for i in (0..<TableSectionStruct.data[cell.currentSection].count).reversed() {
                let currentIndex = NSIndexPath(row: i, section: cell.currentSection)
                TableSectionStruct.mainTableView.deleteRows(at: [currentIndex as IndexPath], with: UITableViewRowAnimation.automatic)
                TableSectionStruct.data[cell.currentSection].remove(at: i)
            }
            TableSectionStruct.mainTableView.endUpdates()
            
            TableSectionStruct.isSectionOpened[cell.currentSection] = false
            
        } else {
            
            TableSectionStruct.data[cell.currentSection] = TableSectionStruct.data02[cell.currentSection]
            
//            for i in 0..<TableSectionStruct.data02[cell.currentSection].count{
//                let indexForChild = ViewControllerTable.childRowChecker(indexrow: i)
//                TableSectionStruct.currentArrow[i-indexForChild] = #imageLiteral(resourceName: "arrowRight.png")
//            }
            
            TableSectionStruct.mainTableView.beginUpdates()
            for i in 0..<TableSectionStruct.data[cell.currentSection].count {
                let currentIndex = NSIndexPath(row: i, section: cell.currentSection)
                TableSectionStruct.mainTableView.insertRows(at: [currentIndex as IndexPath], with: UITableViewRowAnimation.automatic)
            }
            TableSectionStruct.mainTableView.endUpdates()
            
            TableSectionStruct.isSectionOpened[cell.currentSection] = true
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentCell = tableView.cellForRow(at: indexPath)
        
        if TableSectionStruct.openedCell[indexPath.row] == 1 && indexPath.section == 0 {
            TableSectionStruct.openedCell[indexPath.row ] = 0
            
            accordionRemove(indexPath: indexPath, tableView: tableView)
            
            if let arrowedCell = currentCell as? TableSectionViewCell{
                arrowToRight(cell: arrowedCell, indexPath: indexPath.row)
            }
            
        } else if TableSectionStruct.openedCell[indexPath.row] == 0 && indexPath.section == 0 {
            TableSectionStruct.openedCell[indexPath.row] = 1
            
            let test = "test"
            var newIndexpath = indexPath
            
            // tableView.beginUpdates()
            tableView.beginUpdates()
            
            newIndexpath.row = newIndexpath.row + 1
            TableSectionStruct.data[indexPath.section].insert(test, at: newIndexpath.row)
            tableView.insertRows(at: [newIndexpath], with: UITableViewRowAnimation.top)
            TableSectionStruct.openedCell.insert(2, at: newIndexpath.row) // insert child row, code = 2
            
            newIndexpath.row = newIndexpath.row + 1
            TableSectionStruct.data[indexPath.section].insert(test, at: newIndexpath.row)
            tableView.insertRows(at: [newIndexpath], with: UITableViewRowAnimation.top)
            TableSectionStruct.openedCell.insert(3, at: newIndexpath.row) // insert child row, code = 3
            
            newIndexpath.row = newIndexpath.row + 1
            TableSectionStruct.data[indexPath.section].insert(test, at: newIndexpath.row)
            tableView.insertRows(at: [newIndexpath], with: UITableViewRowAnimation.top)
            TableSectionStruct.openedCell.insert(4, at: newIndexpath.row) // insert child row, code = 4
            
            tableView.endUpdates()
            
            if let arrowedCell = currentCell as? TableSectionViewCell{
                arrowToDown(cell: arrowedCell, indexPath: indexPath.row)
            }
            
        }
        
        if indexPath.section == 1 {
            var NSUserDataLatitude = [CGFloat]()
            var NSUserDataLongitude = [CGFloat]()
            
            if let temp01 = PublicStruct.PersistentData.array(forKey: "randomLatitude") as? [CGFloat] {
                NSUserDataLatitude = temp01
            }
            
            if let temp02 = PublicStruct.PersistentData.array(forKey: "randomLongitude") as? [CGFloat] {
                NSUserDataLongitude = temp02
            }
            
            
            classes.tabBarController.selectedIndex = 1
            let tempCenter = CLLocation(latitude: CLLocationDegrees(NSUserDataLatitude[indexPath.row]), longitude: CLLocationDegrees(NSUserDataLongitude[indexPath.row]))
            MapStruct.mapCenter = tempCenter
            MapStruct.indexPath = indexPath.row
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "centermapupdate"), object: self)
            
            PublicStruct.PersistentData.set(NSUserDataLatitude, forKey: "randomLatitude")
            PublicStruct.PersistentData.set(NSUserDataLongitude, forKey: "randomLongitude")
        }
        
        if indexPath.section == 2 {
            insertToContacts(index: indexPath.row)
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if TableSectionStruct.openedCell[indexPath.row] == 0 &&
            indexPath.section == 0{
            if editingStyle == UITableViewCellEditingStyle.delete{
                TableSectionStruct.openedCell.remove(at: indexPath.row)
                PublicStruct.NewEmailStaticData.remove(at: indexPath.row)
                TableSectionStruct.data[0].remove(at: indexPath.row)
                TableSectionStruct.data[1].remove(at: indexPath.row)
                TableSectionStruct.data[2].remove(at: indexPath.row)
                tableView.reloadData()
                
                /////////////////////////////////
                //set data for members
                /////////////////////////////////
                PublicStruct.NewMemberDataIndex = PublicStruct.PersistentData.integer(forKey: PublicStruct.MemberDataIndexKey)
                
                PublicStruct.NewMemberDataIndex = PublicStruct.NewMemberDataIndex - 1
                
                PublicStruct.PersistentData.set(PublicStruct.NewMemberDataIndex, forKey: PublicStruct.MemberDataIndexKey)
                
                for i in 0...(PublicStruct.NewMemberDataIndex-1){
                    TableSectionStruct.data[0][i] = String(i+1)
                }
                
                PublicStruct.NewMemberStaticData = TableSectionStruct.data[0].map{Int($0)!}
                
                PublicStruct.PersistentData.set(PublicStruct.NewMemberStaticData, forKey: PublicStruct.MemberStaticDataKey) // set the index before index is updated
                
                
                /////////////////////////////////
                //set data for email sections
                /////////////////////////////////
                PublicStruct.PersistentData.set(TableSectionStruct.data[1], forKey: PublicStruct.EmailStaticDataKey)
                
                /////////////////////////////////
                //set data for phone sections
                /////////////////////////////////
                PublicStruct.PersistentData.set(TableSectionStruct.data[2], forKey: PublicStruct.PhoneStaticDataKey)
                
                /////////////////////////////////
                //set data for registration date
                /////////////////////////////////
                PublicStruct.NewDate.remove(at: indexPath.row)
                
                PublicStruct.PersistentData.set(PublicStruct.NewDate, forKey: PublicStruct.DateKey)
                /////////////////////////////////
                //set data end
                /////////////////////////////////
            }
        }
    }
}
