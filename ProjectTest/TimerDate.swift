//
//  TimerDate.swift
//  ProjectTest
//
//  Created by Jeriko on 1/3/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

class TimerDate: UIViewController {
    
    var timer = Timer()
    var timer2 = Timer()
    var count = Int()
    var count2 = Int()
    var circleTime = 1.6
    @IBOutlet weak var label01: UILabel!
    @IBOutlet weak var label02: UILabel!
    @IBOutlet weak var circleSecond: CircleTwo!
    @IBAction func openSearchPage(_ sender: UIButton) {
        self.navigationController?.pushViewController(SearchBarController(), animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        timer2 = Timer.scheduledTimer(timeInterval: 0.025, target: self, selector: #selector(updateCircleTimer), userInfo: nil, repeats: true)
        
        label01.layer.cornerRadius = 0.5 * label01.bounds.size.width
        label01.layer.borderColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:1).cgColor as CGColor
        label01.layer.borderWidth = 2.0
        label01.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateTimer(){
        
        count = count + 1
        label01.text = String(count)
        circleTime = 1.5
        if count == 15 {
//            timer.invalidate()
        }
        
        if count >= 2 {
            count2 = count2 + 1
            label02.text = String(count2)
        }
    }
    
    func updateCircleTimer (){
        circleTime = circleTime + 0.05
        circleSecond.defineCircle()
        circleSecond.endAngle = circleTime
        circleSecond.updateCircleView()
        
        if circleTime >= 2.0 {
            circleTime = 0.0
        }
//        label01.bringSubview(toFront: circleClass)
//        label02.bringSubview(toFront: circleClass)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}


class CircleTwo: UIView {
        var circlePath = UIBezierPath()
        var endAngle = 1.4
    
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.backgroundColor = UIColor.clear
        }
    
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
    
    func defineCircle(){
        let circleCenter = CGPoint(x: self.frame.size.height/2, y: self.frame.size.width/2)
        let circleRadius = CGFloat(self.frame.size.width/3)
        circlePath = UIBezierPath(arcCenter: circleCenter,
                                  radius: circleRadius,
                                  startAngle: CGFloat(M_PI * 1.5),
                                  endAngle: CGFloat(M_PI * endAngle),
                                  clockwise: true)
        circlePath.lineWidth = 2.0
        
    }
    
    func updateCircleView (){
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect){
        circlePath.stroke()
    }
}
