//
//  TableSectionViewController.swift
//  ProjectTest
//
//  Created by Jeriko on 12/1/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class TableSectionViewController: UIViewController {
    
    @IBOutlet weak var tableSectionView: UITableView!
    
    let datasource = TableViewDataSource()
    let delegate = TableViewDelegate()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableSectionView.delegate = delegate
        tableSectionView.dataSource = datasource
        
        ViewControllerTable.registerNib(nibName: "ChildCell", forCellReuseIdentifier: "child", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "TableSectionViewCell", forCellReuseIdentifier: "cell", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "EmailCell", forCellReuseIdentifier: "emailcell", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "ChildCellTwo", forCellReuseIdentifier: "childcelltwo", table: tableSectionView)
        
        ViewControllerTable.registerNib(nibName: "TableViewSection", forCellReuseIdentifier: "sectionCell", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "SecondSectionView", forCellReuseIdentifier: "sectioncell", table: tableSectionView)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateSecondSection), name: NSNotification.Name(rawValue: "maplocationupdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toast), name: NSNotification.Name(rawValue: "contactupdated"), object: nil)
        
        if (TableSectionStruct.data[0].count-1) >= 0 {
            for i in 0...(TableSectionStruct.data[0].count-1) {
                TableSectionStruct.openedCell.append(0)
                TableSectionStruct.currentArrow.append(#imageLiteral(resourceName: "arrowRight.png"))
            }
        }
        
        self.title = "Data"
        
        TempData.tempArray = TableSectionStruct.dummyDataCell
        
        //Get all views in the xib
        let allViewsInXibArray = Bundle.main.loadNibNamed("TableViewBG", owner: TableViewBG(), options: nil)
        
        //If you only have one view in the xib and you set it's class to MyView class
        let myView = allViewsInXibArray?.first as! TableViewBG
        
        myView.backgroundGradient()
        tableSectionView.backgroundView = myView
        
        TableSectionStruct.mainTableView = tableSectionView
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if (TableSectionStruct.data[0].count-1) >= 0 {
            for i in (0...(TableSectionStruct.data[0].count)-1).reversed() {
                if TableSectionStruct.data[0][i] == "test" {
                    TableSectionStruct.data[0].remove(at: i)
                }
            }
            
            for i in (0..<TableSectionStruct.openedCell.count).reversed() {
                switch TableSectionStruct.openedCell[i]{
                case 1:
                    TableSectionStruct.openedCell[i] = 0 // set opened state to closed state
                case 2:
                    TableSectionStruct.openedCell.remove(at: i)
                case 3:
                    TableSectionStruct.openedCell.remove(at: i)
                case 4:
                    TableSectionStruct.openedCell.remove(at: i)
                default:
                    break
                }
            }
            
        }
    }
    
    func updateSecondSection (){
        TableSectionStruct.data[1] = MapStruct.homesName
        tableSectionView.reloadData()
    }
    
    func toast (){
        let rect = CGRect(x: (self.view.frame.size.width - self.view.frame.size.width*0.4)/2,
                          y: self.view.frame.size.width*1.4,
                          width: self.view.frame.size.width*0.35,
                          height: self.view.frame.size.height*0.07)
        let toastLabel = UILabel(frame: rect)
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.blue
        toastLabel.textAlignment = NSTextAlignment.center;
        self.view.addSubview(toastLabel)
        
        if TableSectionStruct.contactStatus == true {
            toastLabel.text = "Contact Exist!"
        } else {
            toastLabel.text = "Contact Added"
        }
        
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(
            withDuration: 0.3,
            delay: 1.3,
            options: .curveEaseOut,
            animations: {
                toastLabel.alpha = 0.0
        }, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
