//
//  NCDelegate.swift
//  ProjectTest
//
//  Created by Jeriko on 12/22/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class NCDelegate: NSObject, UINavigationControllerDelegate {
        
    /////////////////////////////////
    //custom transition
    /////////////////////////////////
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CustomTransitionAnimator()
    }
    
}
